An abandoned pinout mapper utility
==================================

I used these tools around 2015 to find a suitable pinout
to design a board for a 100-pin EFM32GG chip that used as many SPI independent, I2C, ADC etc lines.

geck-pins.py extracted data I'd now call pinmux data from vendor PDFs that are not available any more
(with output in ./giantgecko/);
find-overlaps.py contains a hard-coded set of groups desirable for this particular board
and feeds it through the clingo SAT solver to get a pinout solution in which the desired functions do not overlap.

This is not developed any further
(especially given that newer chips' pinmuxes tend to work per-pin and not per-peripheral),
but if you'd like to resurrect it (probably from scratch, but possibly using some lessons learned),
contact <mailto:chrysn@fsfe.org>.
