"""
Extraction of pin positions from EFM32 gecko series datasheets

This tool is part of a very manual work flow (looking up page numbers,
determining page areas, working around glitches) that is fragile (changes in
the datasheets); it is expected that its output will need to be treated as
versioned data source independently.
"""

from __future__ import print_function

import os
import sys
import pickle
from collections import defaultdict
import csv
import natsort
sys.path.append('./pdf-table-extract/src/')
import pdftableextract as pdf

# in inch
bottomhalf = "0:6.5:100:100" # x, y, width, height
topthird = "0:0:100:4"
bottomthird = "0:8:100:100"
tophalf = "0:0:100:6.5"

def memoize(f, *args, **kwargs):
    memo_filename = "memo/%s---%s.pkl"%("--".join(str(x).replace('/', '-') for x in args), "--".join("%s:%s"%(k,v) for k,v in sorted(kwargs.items())))

    try: os.mkdir("memo")
    except OSError: pass

    try:
        return pickle.load(open(memo_filename))
    except IOError:
        pass

    result = f(*args, **kwargs)
    with open(memo_filename, "w") as memo_file:
        pickle.dump(result, memo_file)
    return result

class Chip(object):
    def __init__(self):
        self.routes = defaultdict(lambda:{})
        self.pinouts = defaultdict(lambda:{})

    def set_pinout(self, variantname, pinnumber, pinlabel):
        if pinnumber in self.pinouts[variantname]:
            assert self.pinouts[variantname][pinnumber] == pinlabel
        else:
            self.pinouts[variantname][pinnumber] = pinlabel

    def store(self, filename, table):
        for line in table:
            pin_description = line[0]
            for (route, pin_label) in enumerate(line[1:]):
                if not pin_label:
                    continue
                if route in self.routes[pin_description]:
                    assert self.routes[pin_description][route] == pin_label
                else:
                    self.routes[pin_description][route] = pin_label
        # FIXME this does not yet note which chips have which pin

    def save(self, folder):
        try: os.mkdir(folder)
        except OSError: pass

        for chip, pinout in self.pinouts.items():
            csvname = os.path.join(folder, "%s.csv"%chip)
            with open(csvname, "w") as csvfile:
                csvwriter = csv.writer(csvfile)
                for num, name in natsort.natsorted(pinout.items()):
                    csvwriter.writerow((num, name))

        csvname = os.path.join(folder, "routes.csv")
        with open(csvname, "w") as csvfile:
            csvwriter = csv.writer(csvfile)
            for (use, pins) in natsort.natsorted(self.routes.items()):
                csvwriter.writerow((use,) + tuple(pins.get(i, "") for i in range(max(pins) + 1)))

giantgecko = Chip()

class PageRange(object):
    def __init__(self, firstpage, lastpage, firstpagecrop=None, lastpagecrop=None):
        self.firstpage = firstpage
        self.lastpage = lastpage
        self.firstpagecrop = firstpagecrop
        self.lastpagecrop = lastpagecrop

    def __iter__(self):
        for p in range(self.firstpage, self.lastpage + 1):
            if p == self.firstpage:
                yield str(p), self.firstpagecrop
            elif p == self.lastpage:
                yield str(p), self.lastpagecrop
            else:
                yield str(p), None

class GeckoDatasheet(object):
    def __init__(self, filename, pinout_pages, pinuse_pages):
        self.filename = filename
        self.chipname = filename[6:16].upper()
        self.pinout_pages = pinout_pages
        self.pinuse_pages = pinuse_pages

    def parse(self, pinstore):
        self.parse_pinuse(pinstore)
        self.parse_pinout(pinstore)

    def parse_pinout(self, pinstore):
        if self.pinout_pages is None:
            return
        for (p, pagecrop) in self.pinout_pages:
            cells = memoize(pdf.process_page, self.filename, p, crop=pagecrop, bitmap_resolution=400)
            imax = max(i + u for (i,j,u,v,pg,value) in cells)
            jmax = max(j + v for (i,j,u,v,pg,value) in cells)
            print("pinout of %s %s [%s]: %dx%d (%d)"%(self.filename, p, pagecrop, imax, jmax, len(cells)))

            if imax == 7:
                print("regular pinout")
                table = {}
                for i,j,u,v,pg,value in cells:
                    line = table.setdefault(j, {})
                    assert i not in line
                    line[i] = value
                table = [[v.get(i, None) for i in range(max(v))] for (k,v) in sorted(table.items())]
                assert table[0][0].endswith('Pin# and Name')
                table.pop(0)
                assert table[0][0:2] == ["Pin #", "Pin Name"]
                table.pop(0)
                for line in table:
                    if not any(line[:2]):
                        continue
                    pinnumber = line[0]
                    pinname = line[1]
                    pinstore.set_pinout(self.chipname, pinnumber, pinname)

    def parse_pinuse(self, pinstore):
        print("========", self.chipname)
        for (p, pagecrop) in self.pinuse_pages:
            cells = memoize(pdf.process_page, self.filename, p, crop=pagecrop, bitmap_resolution=400)
            imax = max(i + u for (i,j,u,v,pg,value) in cells)
            jmax = max(j + v for (i,j,u,v,pg,value) in cells)
            if imax == 9:
                print("regular table", self.filename, p, pagecrop, imax, "x", jmax, "cells", len(cells))
                irregs = [(i,j,u,v,pg,value) for (i,j,u,v,pg,value) in cells if (u != 1 or v != 1)]
                regs = [c for c in cells if c not in irregs]
                filtered_irregs = [(i,j,u,v,pg,value) for (i,j,u,v,pg,value) in irregs if value != 'LOCATION' and not value.startswith('Description ')]
                if filtered_irregs:
                    print("irregular cells:", filtered_irregs)
                table = {}
                for i,j,u,v,pg,value in regs:
                    line = table.setdefault(j, {})
                    assert i not in line
                    line[i] = value
                table = [[v.get(i, None) for i in range(max(v))] for (k,v) in sorted(table.items())]
                if table[0][0] == 'Alternate' and not any(table[0][1:]):
                    table.pop(0)
                assert table[0][0] == 'Functionality'
                assert table[0][1:8] == map(str, range(7))
                table.pop(0)

                pinstore.store(self.chipname, table)
            else:
                print("IRREGULAR TABLE", self.filename, p, pagecrop, imax, "x", jmax, "cells", len(cells))
#        if imax != 9 or dumped_one_good == False:
#            dumped_one_good = True
#            ofn = "/tmp/%s-%s"%(filename, p)
#            pdf.core.o_table_html(cells, None, outfile=open(ofn + ".html", "w"))
#            cells = pdf.process_page(filename, p, crop=pagecrop, bitmap_resolution=400, checkcells=True, outfilename=ofn + ".pbm")
#            print("dumped to", ofn, ".{html,pbm}")

datasheets = [
        GeckoDatasheet('d0035_efm32gg230_datasheet.pdf', None, PageRange(47, 51, bottomthird, "0:0:7.65:6.5")),
        GeckoDatasheet('d0036_efm32gg280_datasheet.pdf', PageRange(45, 49, bottomthird, tophalf), PageRange(49, 55, bottomhalf, "0:0:7.65:4.5")),
        GeckoDatasheet('d0126_efm32gg332_datasheet.pdf', PageRange(45, 48, bottomthird, topthird), PageRange(48, 52, bottomhalf, topthird)),
        GeckoDatasheet('d0039_efm32gg380_datasheet.pdf', PageRange(45, 49, bottomthird, tophalf), PageRange(49, 55, bottomhalf, topthird)),
        GeckoDatasheet('d0080_efm32gg995_datasheet.pdf', PageRange(46, 50, bottomthird), PageRange(51, 58)),
        ]

# data that's garbled in page transitions
giantgecko.pinouts['EFM32GG280']['3'] = 'PA2'
giantgecko.pinouts['EFM32GG332']['3'] = 'PA2'
giantgecko.pinouts['EFM32GG332']['29'] = 'PD1'
giantgecko.pinouts['EFM32GG380']['3'] = 'PA2'
giantgecko.pinouts['EFM32GG995']['A3'] = 'PE12'
giantgecko.pinouts['EFM32GG995']['C10'] = 'PF0'
giantgecko.pinouts['EFM32GG995']['H2'] = 'PB6'
giantgecko.pinouts['EFM32GG995']['L13'] = 'PD7'

# missing in EFM32GG332
giantgecko.routes['US2_TX'] = {0: 'PC2'}

for d in datasheets:
    d.parse(giantgecko)

giantgecko.save('giantgecko')
