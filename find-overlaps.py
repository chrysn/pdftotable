"""Using the output of gecko-pins.py, determine for a hard-coded set of pin
functions which have a chance of working out"""

# this was written as pytho3 but works with pypy2 as well
from __future__ import print_function

from collections import defaultdict
import itertools
import csv
from pprint import pprint
import subprocess

routes = {pinandroutes[0]: pinandroutes[1:] for pinandroutes in csv.reader(open('./giantgecko/routes.csv'))}

pin_numbers = {label: number for (number, label) in csv.reader(open('./giantgecko/EFM32GG380.csv'))}

routes = {pin: [(p if p in pin_numbers else None) for p in r] for (pin, r) in routes.items()}

class Sentinel(str):
    pass

MATCHSTART = Sentinel("MATCHSTART")

groups = {
        'HFXTAL': MATCHSTART,
        'LFXTAL': MATCHSTART,
        'DBG': MATCHSTART,
        'USB': ['USB_DM', 'USB_DP', 'USB_ID'], # skipping VREG also because of the USV_VREGO typo in the manuals
        'BU': ['BU_VIN'],

        'I2C0': MATCHSTART,
        'I2C0__2': MATCHSTART, # we should be able to do that, but it costs computation time without end due to the high number of routes available
        #'I2C0__3': MATCHSTART, # disabled to give a em4 wakeup a chance
        'I2C1': MATCHSTART,
        'I2C1__2': MATCHSTART,

        'US0': MATCHSTART, # we could sacrifice CS
        # 'US0__2': MATCHSTART, # dropped because of gpio pin shortage
        'US1': MATCHSTART,
        #'US1__2': MATCHSTART, # six usart routes seem to be impossible
        'US2': MATCHSTART,
        # 'US2__2': MATCHSTART, # dropped because of gpio pin shortage

        'U0': MATCHSTART,
        'U0__2': MATCHSTART,
        'U1': MATCHSTART,
        'U1__2': MATCHSTART,
        'LEU0': MATCHSTART,
        #'LEU0__2': MATCHSTART,
        'LEU1': MATCHSTART,
        #'LEU1__2': MATCHSTART,

        # LEDs: would work on other TIM too, but not on TIM0 where it was on singleboard
        'TIM1_CC': MATCHSTART,
}

ONEMOREROUTE = ('__2', '__3')
def spell_out_matchstart(groups):
    for k, v in groups.items():
        if v is not MATCHSTART:
            continue
        startfrag = k
        for o in ONEMOREROUTE:
            if startfrag.endswith(o):
                startfrag = startfrag[:-len(o)]
        groups[k] = [f for f in routes.keys() if f.startswith(startfrag)]
    return groups

groups = spell_out_matchstart(groups)

def find_equivalent_groups(groups):
    classes = defaultdict(lambda: set())
    for g in groups:
        for o in ONEMOREROUTE:
            if g.endswith(o):
                classes[g[:-len(o)]].add(g)
                break
        else:
            classes[g].add(g)
    result = set()
    for c, items in classes.items():
        if len(items) <= 1:
            continue
        sample = tuple(items)[0]
        if all(groups[i] == groups[sample] for i in items):
            result.add(tuple(sorted(set(items)))) # sorting to keep the result stable
    return result
equivalentgroups = find_equivalent_groups(groups)

grouproutes = {}
for k, v in groups.items():
    pinroutes = [routes[p] for p in v]
    grouproutes[k] = {}
    for i in range(max(len(r) for r in pinroutes)):
        routepins = [r[i] if i < len(r) else None for r in pinroutes]
        if any(p is None for p in routepins):
            continue

        grouproutes[k][i] = routepins

# enbling this seems to conflict with a second route from USART2
grouproutes['opamp'] = {
        # the "two opamp diffeerential amplifier" from the datasheets using opamp 0 and 1, also the "three opamp d.a."
        0: [routes['DAC0_P1 / OPAMP_P1'][0], routes['DAC0_P0 / OPAMP_P0'][0]],
        # the "two opamp diffeerential amplifier" from the datasheets using opamp 1 and 2
        1: [routes['DAC0_P1 / OPAMP_P1'][0], routes['DAC0_P2 / OPAMP_P2'][0]],
        }
groups['opamp'] = ['da_N', 'da_P'] # here, it's only used for providing labels
wakeuproutes = dict(enumerate(sorted([routes[label][0]] for label in routes if label.startswith('GPIO_EM4WU'))))
grouproutes['em4_I'] = wakeuproutes
grouproutes['em4_II'] = wakeuproutes
grouproutes['em4_III'] = wakeuproutes
equivalentgroups.add(('em4_I', 'em4_II', 'em4_III'))

adcroutes = dict(enumerate(sorted([routes[label][0]] for label in routes if label.startswith('ADC0_CH'))))
grouproutes['adc_I'] = adcroutes
grouproutes['adc_II'] = adcroutes
grouproutes['adc_III'] = adcroutes
equivalentgroups.add(('adc_I', 'adc_II', 'adc_III'))

def solve_manually():
    valid_combinations = []

    pin_number = sum(max(len(v) for v in v_choices.values()) for v_choices in grouproutes.values()) # max should work as well as min, they should be equal
    group_names, group_routes = zip(*((k, v.keys()) for (k,v) in grouproutes.items()))
    # how bad can combinatorial explosion be...
    for variation in itertools.product(*group_routes):
        used_pins = set()
        for g, v in zip(group_names, variation):
            used_pins.update(grouproutes[g][v])
        if pin_number == len(used_pins):
            valid_combinations.append(dict(zip(group_names, variation)))
            break

    print("%d valid combinations found"%len(valid_combinations))

def solve_using_clingo():
    asplines = []

    groupname2token = lambda group, route: "r_%s_%d"%(group, route)
    # "all functions implemented"
    for g in grouproutes:
        asplines.append("1 {" + "; ".join(groupname2token(g, i) for i in grouproutes[g].keys()) + "} 1 .")
    # "used at most in one pin"
    for p in pin_numbers:
        used_in = [(g, r) for (g, rs) in grouproutes.items() for (r, rpins) in rs.items() if p in rpins]
        if not used_in:
            continue
        asplines.append("0 {" + "; ".join(groupname2token(g, r) for (g,r) in used_in) + "} 1 .")
    # "in equivalent groups, the routes chosen must be ascending" (first member of equivalent class has lowest route number)
    # if i knew more than a single construct in this language, i might express this in a more clever way
    for e in equivalentgroups:
        for e1, e2 in zip(e[:-1], e[1:]): # pairwise smaller-than
            routes = grouproutes[e1].keys()
            assert routes == grouproutes[e2].keys()
            bad = {i: [j for j in routes if j > i] for i in routes}
            for i, js in bad.items():
                for j in js:
                    asplines.append("{" + groupname2token(e1, i) + ";" + groupname2token(e2, j) + "} 1 .")

    # workaround for http://sourceforge.net/p/potassco/bugs/113/
    p = subprocess.Popen("gringo | clasp 0", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    #p = subprocess.Popen(["clingo", "-", "0"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    out, err = p.communicate("\n".join(asplines).encode('utf8'))
    p.wait()
    if p.returncode < 0:
        print("clingo run was probably incomplete")

    lines = out.decode('utf8').split('\n')
    line_iter = lines.__iter__()
    answerlines = []
    for line in line_iter:
        if line.startswith('Answer: '):
            answerlines.append(line_iter.__next__())

    def token2groupname(token):
        assert token.startswith('r_')
        groupname = token[2:token.rindex('_')]
        route = int(token[token.rindex('_')+1:])
        return groupname, route
    pinouts = []
    for al in answerlines:
        pinout = {}
        routes = map(token2groupname, al.split())
        for groupname, route in routes:
            labels = groups.get(groupname, [groupname])
            pins = grouproutes[groupname][route]
            assert len(labels) == len(pins)
            for pin, label in zip(pins, labels):
                assert pin not in pinout
                pinout[pin] = "%s#%d"%(label, route) if (len(pins) > 1 and len(grouproutes[groupname]) > 1) else label
        pinouts.append(pinout)
    # provide deterministic results
    pinouts = sorted(pinouts, key=lambda x: tuple(sorted(x.items())))

    # chose and make usable first pinout
    samplepinout = pinouts[0]

    print("Sample sed expression for .src files")
    print("sed " + ' '.join("-e 's~\\t%s$~\\t%s %s~g'"%(k, k, v) for (k,v) in samplepinout.items()))
    print("For reference, the pinout is")
    print(samplepinout)

    # ease viewing
    static_pinout = {}
    for pin_name in list(samplepinout.keys()):
        if all(p.get(pin_name, None) == samplepinout.get(pin_name, None) for p in pinouts):
            static_pinout[pin_name] = samplepinout[pin_name]
            for p in pinouts:
                if pin_name in p:
                    del p[pin_name]
    print("Common pins:")
    pprint(static_pinout)
    print("Variable pins:")
    for p in pinouts:
        pprint(p, compact=True, width=100000)

#solve_manually()
solve_using_clingo()
